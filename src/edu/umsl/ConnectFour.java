//Emily Beauto 
//CS 2261 Project 2
//Connect 4

package edu.umsl;

import java.util.Scanner;

public class ConnectFour 
{
	//describes the array need for the game board 
	static char[][] board;
	
	
	//create a blank board with space fillers 
	public static void createBoard()
	{
		board = new char[6][7];
		for(int r = 0; r < board.length; r++)
		{
			for(int c = 0; c < board[r].length; c++ )
			{
				board[r][c] = ' ';
			}
		}
	}
	
	//determines when board is filled
	public static boolean isOver()
	{
		for(int i = 0; i < board[0].length; i++)
		{
			if(board[0][i] == ' ')
			{
				return false;
			}
		}
		return true;
	}
	
	//print the game board with pretty formating 
	public static void printBoard() 
	{
		for(int i = 0; i < board[0].length; i++)
		{
			System.out.printf("%2d",i);
		}
		System.out.println();
		for(int r = 0; r < board.length; r++)
		{
			//pretty formating
			System.out.printf("|");
			for(int c = 0; c < board[r].length; c++ )
			{
				System.out.printf("%c|", board[r][c]);
			}
			System.out.println();
		}
		System.out.println("---------------");
		System.out.println();
	}
	
	//drops in the players specific token in the column
	public static boolean drop(char token, int column)
	{
		//check if column is in bounds
		if(column < 0 || column > 6)
		{
			System.out.println("That column doesn't exist please try again.");
		}
		boolean place = false;
		for(int r = board.length - 1; r >= 0; r--)
		{
			//check for empty place
			if(board[r][column] == ' ')
			{
				board[r][column] = token;
				place = true;
				break;
			}
		}
		return place;
	}
	
	public static boolean winCheck()
	{
		//four specific cases where you can win so four separate flags, and pull winning token
		boolean horizontal = false;
		boolean vertical = false;
		boolean forwardSlash = false;
		boolean backwardSlash = false;
		char winningToken = ' ';
		
		//first win check type horizontal; check for four in a row
		for(int r = 0; r <= 5; r++)//numbers for start have to be left side of the graph vertically
		{
			for(int c = 0; c <= 3; c++)
			{
				if (board[r][c] == board[r][c + 1] && board[r][c] == board[r][c + 2] && board[r][c] == board[r][c + 3] && board[r][c] != ' ')
				{
					horizontal = true;
					winningToken = board[r][c];
					break;
				}
			}
			
		}
		
		//second win check type vertical; check for four in a column
		for(int r = 0; r <= 2; r++)//numbers for the start have to be top half of the graph horizontally 
		{
			for(int c = 0; c <= 6; c++)
			{
				if (board[r][c] == board[r + 1][c] && board[r][c] == board[r + 2][c] && board[r][c] == board[r + 3][c] && board[r][c] != ' ')
				{
					vertical = true;
					winningToken = board[r][c];
					break;
				}
			}
		}
		
		//third win check type forwardSlash; check for four in diagonal or have one and move to the bottom left
		for (int r = 0; r <= 2; r++) //numbers for the start being in top right quadrant
        {
            for (int c = 3; c <= 6; c++) 
            {
                if(board[r][c] == board[r + 1][c - 1] && board[r][c] == board[r + 2][c - 2] && board[r][c] == board[r + 3][c - 3] && board[r][c] != ' ')
				{
                	forwardSlash = true;
					winningToken = board[r][c];
					break;
				}
            }
        }
		
		//fourth win check type backSlash;  check for four in diagonal or have one and move bottom right
		for (int r = 0; r <= 2; r++) //numbers for the start being in top left quadrant
        {
            for (int c = 0; c <= 3; c++) 
            {
                if(board[r][c] == board[r + 1][c +1] && board[r][c] == board[r + 2][c + 2] && board[r][c] == board[r + 3][c + 3] && board[r][c] != ' ')
				{
                	backwardSlash = true;
					winningToken = board[r][c];
					break;
				}
            }
        }
		
		//print out winner if there is
		if(horizontal || vertical || forwardSlash || backwardSlash)
		{
			System.out.println("Player " + winningToken + " has won this game. Congrats!");
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//driver program
	public static void main(String[] arg) 
	{
		//call to create board, create scanner and flag for the turns
		createBoard();
		Scanner input = new Scanner(System.in);
		//keep track of the player; true is red and false is yellow
		boolean flag = false;
		
		//flip flop players, print board, create token 'R' or 'Y'
		do
		{
			//flip flop players
			flag = !flag;
			printBoard();
			char token; 
			
			if(flag)
			{
				token = 'R';
				System.out.println("Please choose a column (0-6) to drop the RED disk: ");
			}
			else
			{
				token = 'Y';
				System.out.println("Please choose a column (0-6) to drop the YELLOW disk: ");
			}
			boolean gameStatus = false;
			while(!gameStatus)
			{
				try
				{
					gameStatus = drop(token, input.nextInt());
					if (!gameStatus) 
					{
						System.out.println("Invalid position or Position is already filled. Please try again: ");
	                }
	            } 
				catch (Exception e) 
				{
	            	System.out.println("Invalid input. Try Again: ");
	                input.nextLine();
	            }
			}
			System.out.println();
		}while(!isOver() && !winCheck());  //while board isn't full and there is no wins
		
		//code for no winners
		printBoard();
		if(!winCheck())
		{
			System.out.println("There are no winners and the game is now over.");
			System.out.println("Thank you for playing!");
		}
		
		input.close();
	}
}